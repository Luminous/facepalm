
/**
 * Module dependencies.
 */

// res.render() vs res.sendfile()

var express = require('express');
var routes = require('./routes');
var http = require('http');
var path = require('path');
var url = require('url');
var mongo = require('mongodb');
var mongoStore = require('connect-mongo')(express);

var app = express();

var DB = require('./db');

// all environments
app.set('port', process.env.PORT || 3333);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(express.query());
app.use(express.cookieParser('1234567890'));
app.use(express.session(
    {
        store : new mongoStore(
            {
                url : 'mongodb://localhost:27017',
                collection : 'session',
                db : 'swpdb'
            }),
        secret : '1234567890'
    }
));
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

/*
app.use(express.basicAuth(function(user, pass, callback)
{
    var result = (user === 'testUser' && pass === 'testPass');
    callback(null, result);
}
));
*/
/*
app.use(function(req, res, next)
{
res.send('logger ', req.method, res.url);
});
*/

var mongoserver = new mongo.Server('localhost', 27017);
var db = new mongo.Db('facepalm', mongoserver, {safe : false});

app.get('/', function(req, res, next)
{
    console.log('%s %s', req.method, req.url);
    next();
});

app.get('/', routes.index);

app.get('/hello', function(req, res)
{
	var body = 'jello wörld';
	res.setHeader('Content-Type', 'text/plain');
	res.setHeader('Content-Length', body.length);
	res.end(body);
});
/*
app.get('/login', function(req, res)
{
   res.render('login');
});

app.get('/logout', function(req, res)
{
    req.session.destroy(function()
    {
        res.redirect('/');
    });
});
*/

// Error handling
/*
app.use(function(err, req, res, next){
    console.error(err.stack);
    res.send(500, 'Something broke!');
});
*/
/*
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(function(err, req, res, next){
// logic
});
*/

// In case neccessary

/*
app.use(function(req, res, next){
    res.send(404, 'Sorry cant find that!');
});
*/

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
