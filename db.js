/**
 * Created by erik on 2013-11-06.
 */
/**
 * Created by erik on 2013-10-30.
 */

// needs abstraction?

exports.dbop = function dbop(op)
{
    if (dbopen)
    {
        db.collection('msgentries', function(err, collection)
        {
            if (!err)
            {
                op(db, collection);
            }
        });
    }
    else
    {
        db.open(function(err, db)
        {
            if (!err)
            {
                dbopen = true;

                db.collection('msgentries', function(err, collection)
                {
                    if (!err)
                    {
                        op(db, collection);
                    }
                });
            }
        });
    }
};

exports.save = function save(response, request)
{
    var getVars = url.parse(request.url, true);
    var tweetString = getVars.query['msg'];

    server.dbop(function(db, collection)
    {
        collection.insert({ message : "something else", flag : false }, function(err, doc)
        {
            if (!err)
            {
                var res = doc[0]._id;
                console.log("success, _id is now:", res);
            }
            else
            {
                var res = undefined;
                console.log("Something went to shit early on");
            }

            if (res != undefined)
            {
                response.writeHead(200, {'Content-Type':'text/html'});
                response.write("save successful, db object got _id "+res);
                response.end();
            }
            else if (answer == 1)
            {
                console.log("Error 500");
                response.writeHead(500, {'Content-Type':'text/html'});
                response.write("Error 500: Stay tuned while we fix");
                response.end();
            }
            else
            {
                console.log("Internal error: saving to db failed!")
            }
        });
    });
}

exports.getall = function getall(response, request)
{
    server.dbop(function(db, collection)
    {
        collection.find(function(err, result)
        {
            if (!err)
            {
                result.toArray(function(err, listed)
                {
                    if (!err)
                    {
                        console.log("get succeeded, got ", listed);
                        var res = JSON.stringify(listed);

                        console.log(res);
                        var res = res.toString();
                        console.log(res);

                        if (res != undefined)
                        {
                            response.writeHead(200, {'Content-Type':'application/json'});
                            response.write(res);
                            response.end();
                        }
                        else if (answer == 1)
                        {
                            console.log("Error 500");
                            response.writeHead(500, {'Content-Type':'text/html'});
                            response.write("Error 500: Stay tuned while we fix");
                            response.end();
                        }
                        else
                        {
                            console.log("Internal error: saving to db failed!")
                        }

                    }
                    else
                    {
                        console.log("json processing went to shit");
                    }
                });

            }
            else
            {
                console.log("crashing and burning, dying horribly ...");
            }

        });
    });
}

exports.flag = function flag(response, request)
{
    var getVars = url.parse(request.url, true);
    var idString = getVars.query['ID'];

    var ObjectID = new mongo.BSONPure.ObjectID.createFromHexString(idString);

    server.dbop(function(db, collection)
    {
        collection.findOne({ _id : ObjectID }, function(err, doc)
        {
            if (!err)
            {
                var res = doc;

                if(res.flag == false)
                {
                    var flagged = false;
                }
                else
                {
                    var flagged = true;
                }
                console.log("found object", doc);

            }
            else
            {
                console.log("crashing and burning, dying horribly ...");
            }

            if (res != undefined)
            {

                if(!flagged)
                {
                    collection.update({ _id : ObjectID },
                        {
                            $set:
                            {
                                flag : true
                            }
                        });
                }
                else
                {
                    collection.update({ _id : ObjectID },
                        {
                            $set:
                            {
                                flag : false
                            }
                        });
                }

                response.writeHead(200, {'Content-Type':'text/html'});
                response.write("flag successful");
                response.end();

            }
            else if (answer == 1)
            {
                console.log("Error 500");
                response.writeHead(500, {'Content-Type':'text/html'});
                response.write("Error 500: Stay tuned while we fix");
                response.end();
            }
            else
            {
                console.log("Internal error: flagsetting failed!")
            }
        });
    });
}