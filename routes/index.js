/**
 * Created by erik on 2013-11-06.
 */

exports.index = function(req, res){
    res.render('index', { title: 'FacePalm' });
};

exports.profile = function(req, res){
    res.render('profile', { title: '\'s profile' });
};

exports.search = function(req, res){
    res.render('search', { title: 'User search summary' });
};

exports.signup = function(req, res)
{
    res.render('signup', { title: 'User search summary'});
};

exports.users = function(req, res){
    res.send("");
};

exports.login = function(req, res, next){
    if(req.session.user)
    {
        next();
    }
    else
    {
        req.session.error = 'Access denied';
        res.redirect('/login');
    }
    res.render('login', { title: 'Sign in' });
};

/*app.options(req, res)
 res.header('Access-Control-Allow-Origin', '*');
 res.header('Access-Control-Allow-Credentials', true);
 res.header('Access-Control-Allow-Methods', 'POST', 'GET', 'PUT', 'DELETE', 'OPTIONS');
 res.header('Access-Control-Allow-Headers', 'Content-Type');*/
/*
 exports.dbop = function(req, res, db)
 {
 db.collection('', );
 };*/